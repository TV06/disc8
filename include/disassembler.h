#ifndef DASM_H
#define DASM_H

#include <stdint.h>

/* We only care about ROM size. Addresses from 0x000 to 0x1FF don't matter now */
#define DASM_SIZE_ROM_MAX    (4096U - 512U)
/* Start address of programs. Used only for printing purposes */
#define DASM_PROG_ADDR_START (0x200U)


/* CHIP-8 Disassembler */
typedef struct DC8ROM {
    uint8_t*    buffer; // ROM contents
    long        size; // ROM size (B)
} DC8ROM;


/* Array for instructions [0, F] */
extern void (*translate[5])(uint16_t* opcode, char* assm);


/* Load ROM contents into file buffer */
int8_t dasm_load(DC8ROM* rom, const char* file);

/* Decodes ROM's raw opcodes. Writes to output */
void dasm_raw(DC8ROM* rom, const char* output);

/* Decodes and translates ROM's raw opcodes into assembly-like code. Writes to output */
void dasm_asm(DC8ROM* rom, const char* output);

#endif // DASM_H