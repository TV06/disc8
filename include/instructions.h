#ifndef IS_H
#define IS_H

#include <stdio.h>
#include <stdint.h>


/* Function for translating 0-starting instructions */
void get_instruction_0(uint16_t* opcode, char* assm);

/* Function for translating [1, 7] and [9, D] instructions */
void get_instruction(uint16_t* opcode, char* assm);

/* Function for translating 8-starting instructions */
void get_instruction_8(uint16_t* opcode, char* assm);

/* Function for translating E-starting instructions */
void get_instruction_e(uint16_t* opcode, char* assm);

/* Function for translating F-starting instructions */
void get_instruction_f(uint16_t* opcode, char* assm);

#endif // IS_H