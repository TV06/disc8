# disC8
A CHIP-8 disassembler built in C

disCHIP-8 is a disassembler tool for CHIP-8 programs. It converts CHIP-8 bytecode into 'assembly' instructions.

## Installation

1. Clone the repository
```bash
    git clone https://gitlab.com/T0V6/disc8.git
```

2. Build source code
```bash
    make
```

3. Delete build
```bash
    make clean
```

## Usage
* From project's folder
```bash
    ./bin/disc8 <rom path> <format> <output>
```
* Parameters
    * <_rom path_>: path to the desired ROM
    * <_format_>: how the disassembler should behave
        * --raw: hexadecimal
        * --asm: assembly-like
        * --all: raw & asm
    * <_output_>: where the code should be written into
        * -c: outputs to console. It's the default output.
        * -f <_file path_>: outputs to the specified file. ALWAYS APPENDS!

    

## Documentation
The source code contains developer comments explaining disassembler functionality. This currently serves as the project's sole documentation.

## Contributing

Bugs, proposals, or feature pull requests are welcome :D

## License

This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/). See the [LICENSE](LICENSE) file for details.