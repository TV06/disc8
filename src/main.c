#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "disassembler.h"


/* 
    USAGE:
            ./bin/disc8 <ROM FILEPATH> <format> <output>

    ARGS:
        FORMAT:
                * --raw     (hexadecimal)
                * --asm     (assembly-like)
                * --all     (raw & asm. Will write to the same output!)
                
        OUTPUT:
                * -c                    (outputs to console; default)
                * -f <FILE PATH>        (outputs to file, ALWAYS APPENDS!)
        
*/

/*
    TODO:
        * asm
            - Place labels on function calls
*/




int main(int argc, char** argv)
{
    DC8ROM rom;
    

    if (argc < 4) {
        fprintf(stderr, "[MAIN] Error: missing parameters!\n");
        printf("Usage: ./bin/disc8 <ROM FILEPATH> <format> <output>\n");

        return -1;
    }



    /* Parse ROM's filepath */
    const char* file = argv[1]; // rom's filepath



    /* Parse output */
    const char* output_stream = NULL;

    if (strcmp(argv[3], "-f") == 0) {
        if (argc != 5) {
            fprintf(stderr, "[MAIN] Error: missing parameters! Couldn't find filepath for %s output\n", argv[3]);
            printf("Usage for file output: -f <FILE PATH>\n");

            return -1;
        }

        output_stream = argv[4]; // file path
    }
    else {
        // defaults to console 
        output_stream = "console";
    }



    /* Load ROM */
    if (dasm_load(&rom, file)) {
        fprintf(stderr, "[MAIN] Error: ROM loading failed!\n");

        return -1;
    }



    /* Parse format */
    if (strcmp(argv[2], "--raw") == 0)          { dasm_raw(&rom, output_stream); }
    else if (strcmp(argv[2], "--asm") == 0)     { dasm_asm(&rom, output_stream); }
    else if (strcmp(argv[2], "--all") == 0) {
        dasm_raw(&rom, output_stream);
        dasm_asm(&rom, output_stream);
    }
    else {
        fprintf(stderr, "[MAIN] Error: %s is not a valid format", argv[2]);

        return -1;
    }


    return 0;
}