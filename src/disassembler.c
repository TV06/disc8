#include <stdlib.h>
#include <string.h>
#include "disassembler.h"
#include "instructions.h"


void (*translate[5])(uint16_t* opcode, char* assm) = { NULL };


int8_t dasm_load(DC8ROM* rom, const char* file)
{
    /*
        Random text from previous load implementation:

        Stupid double pointers >:c

        **rom   == value
        *rom    == rom (main; address of value)
        rom     == &rom (main; address of main's rom)


                         addr                         addr                
                     +------------+     *d_ptr    +------------+   **d_ptr      +-------+
        d_ptr ---->  |   ptr      |     ------>   |   value    |   ------->     |  10   |
                     +------------+               +------------+                +-------+

    */
   
    printf("[MEMORY] Loading ROM");

    FILE* f = fopen(file, "rb");

    if (f) {
        printf("[MEMORY] opened file %s\n", file);

        if (fseek(f, 0, SEEK_END) != 0) {
            fclose(f);
            fprintf(stderr, "[MEMORY] Error seeking ROM's file end\n");

            return -1;
        }

        rom->size = ftell(f);   // get file size
        
        printf("[MEMORY] ROM SIZE IS %ldB\n", rom->size);

        if ((rom->size) > DASM_SIZE_ROM_MAX) { 
            printf("[MEMORY] ## WARNING! ROM SIZE EXCEEDS THE ADDRESS SPACE FOR A CHIP-8 PROGRAM\n");
        }

        // this is ugly af, redo
        rom->buffer = ((rom->size) > 0) ? (uint8_t*)malloc((size_t)(rom->size)) : NULL; // so we don't restrict it to CHIP-8's 3584B limit 
        if (rom->buffer) {
            rewind(f); // go to the beginning of the file

            size_t bytes_read = fread(rom->buffer, 1, (size_t)(rom->size), f); // second param is size in bytes of each element
            
            if (bytes_read != (size_t)(rom->size)) {
                fclose(f);
                fprintf(stderr, "[MEMORY] Error filling temp buffer from ROM\n");

                return -1;
            }

            printf("[MEMORY] ROM loaded successfully\n");
        }
        else {
            fclose(f);
            fprintf(stderr, "[MEMORY] Error allocating memory for ROM buffer: <rom> was NULL\n");

            return -1;
        }
    }
    else { 
        fprintf(stderr, "[MEMORY] Error opening ROM file\n");

        return -1; 
    }


    fclose(f);


    /* Load function pointers */
    printf("[MEMORY] Loading instructions function pointers\n");
    translate[0]    = &get_instruction_0;
    for (uint8_t i = 1; i <= 7; ++i)    { translate[i] = &get_instruction; }
    translate[8]    = &get_instruction_8;
    for (uint8_t i  = 9; i <= 0xD; ++i)  { translate[i] = &get_instruction; }
    translate[0xE]  = &get_instruction_e;
    translate[0xF]  = &get_instruction_f;
    printf("[MEMORY] Instructions loaded successfully\n");


    return 0;
}


void dasm_raw(DC8ROM* rom, const char* output)
{
    FILE* stream = NULL;

    if (strcmp(output, "console") == 0) {
        stream = stdout;
    }
    else {
        stream = fopen(output, "a"); // append

        if(!stream) {
            fclose(stream);
            fprintf(stderr, "[DASM] Error opening file: output stream was null!\n");

            return;
        }
    }



    fprintf(stream, "\n==========================\n");
    fprintf(stream, " ROM's RAW CHIP-8 OPCODES\n");
    fprintf(stream, "==========================\n");
                                                   
    fprintf(stream, " _______________ \n");
    fprintf(stream, "|ADDR       OPCD|\n");
    fprintf(stream, "|------| |------|\n");
   for (uint32_t i = 0; i < (uint32_t)rom->size; i += 2) {
        fprintf(stream, "|               |\n");
        fprintf(stream, "|%#X  ->  ", DASM_PROG_ADDR_START + i);

        for (int j = 0; j < 2; ++j)     { fprintf(stream, "%02X", rom->buffer[i+j]); }

        fprintf(stream, "|\n|_______________|\n");
    }
    fprintf(stream, "\n");


    if (stream != stdout) { 
        printf("[DASM] Disassembled program successfully saved at %s\n", output);
        fclose(stream); 
    }


    return;
}


void dasm_asm(DC8ROM* rom, const char* output)
{
    FILE* stream = NULL;

    if (strcmp(output, "console") == 0) {
        stream = stdout;
    }
    else {
        stream = fopen(output, "a"); // append

        if(!stream) {
            fclose(stream);
            fprintf(stderr, "[DASM] Error opening file: output stream was null!\n");

            return;
        }
    }

    /* shit gets spacing wrong when outputting to a file */

    char assm[21] = { 0 };

    fprintf(stream, "\n================================\n");
    fprintf(stream, "   ROM's CHIP-8 ASSEMBLY CODE\n");
    fprintf(stream, "================================\n");
    fprintf(stream, "--------------------------------\n");
    fprintf(stream, " ADDR |\tMNEM |\tOPRD\n");
    fprintf(stream, "--------------------------------\n");
    for (uint32_t i = 0; i < (uint32_t)rom->size; i += 2) {
        fprintf(stream, "%#X", DASM_PROG_ADDR_START + i);
        uint16_t opcode     = (rom->buffer[i] << 8U) | rom->buffer[i+1];
        uint8_t ins_index   = (opcode & 0xF000U) >> 12U;

        translate[ins_index](&opcode, assm);

        fprintf(stream, "%s", assm);
        fprintf(stream, "--------------------------------\n");
    }
    fprintf(stream, "\n");
    

    if (stream != stdout) { 
        printf("[DASM] Disassembled program successfully saved at %s\n", output);
        fclose(stream); 
    }


    return;
}