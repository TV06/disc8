#include "instructions.h"


void get_instruction_0(uint16_t* opcode, char* assm)
{    
    switch (*opcode & 0x0F00U)
    {
        case 0x0000:
            switch (*opcode & 0x00FFU)
            {
            case 0x00E0:
                sprintf(assm, "\tCLS\n");
                break;
            
            case 0x00EE:
                sprintf(assm, "\tRET\n");
                break;
            
            default:
                sprintf(assm, "\tUNK=%04X\n", *opcode);
                break;
            }
            break;

        default:
            sprintf(assm, "\tSYS\t%#03X\n", *opcode & 0x0FFFU);
            break;
    }


    return;
}


void get_instruction(uint16_t* opcode, char* assm)
{
    switch (*opcode & 0xF000U)
    {
    case 0x1000:
        sprintf(assm, "\tJP\t%#04X\n", *opcode & 0x0FFFU);
        break;

    case 0x2000:
        sprintf(assm, "\tCALL\t%#04X\n", *opcode & 0x0FFFU);
        break;

    case 0x3000:
        sprintf(assm, "\tSE\tV%u, %d\n", (*opcode & 0x0F00U) >> 8U, *opcode & 0x00FFU);
        break;

    case 0x4000:
        sprintf(assm, "\tSNE\tV%u, %d\n", (*opcode & 0x0F00U) >> 8U, *opcode & 0x00FFU);
        break;

    case 0x5000:
        sprintf(assm, "\tSE\tV%u, V%u\n", (*opcode & 0x0F00) >> 8U, (*opcode & 0x00F0) >> 4U);
        break;

    case 0x6000:
        sprintf(assm, "\tLD\tV%u, %d\n", (*opcode & 0x0F00U) >> 8U, *opcode & 0x00FFU);
        break;
    
    case 0x7000:
        sprintf(assm, "\tADD\tV%u, %d\n", (*opcode & 0x0F00U) >> 8U, *opcode & 0x00FFU);
        break;

    case 0x9000:
        sprintf(assm, "\tSNE\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0xA000:
        sprintf(assm, "\tLD\tINDEX, %#04X\n", *opcode & 0x0FFFU);
        break;

    case 0xB000:
        sprintf(assm, "\tJP\tV0, %#04X\n", *opcode & 0x0FFFU);
        break;

    case 0xC000:
        sprintf(assm, "\tRND\tV%u, %u\n", (*opcode & 0x0F00U) >> 8U, *opcode & 0x00FFU);
        break;

    case 0xD000:
        sprintf(assm, "\tDRW\tV%u, V%u, %u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U, *opcode & 0x000FU);
        break;

    default:
        sprintf(assm, "\tUNK=%04X\n", *opcode);
        break;
    }


    return;
}


void get_instruction_8(uint16_t* opcode, char* assm)
{
    switch (*opcode & 0x000FU)
    {
    case 0x0000:
        sprintf(assm, "\tLD\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0x0001:
        sprintf(assm, "\tOR\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0x0002:
        sprintf(assm, "\tAND\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0x0003:
        sprintf(assm, "\tXOR\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0x0004:
        sprintf(assm, "\tADD\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0x0005:
        sprintf(assm, "\tSUB\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0x0006:
        sprintf(assm, "\tSHR\tV%u\n", (*opcode & 0x0F00U) >> 8U);
        break;
    
    case 0x0007:
        sprintf(assm, "\tSUBN\tV%u, V%u\n", (*opcode & 0x0F00U) >> 8U, (*opcode & 0x00F0U) >> 4U);
        break;

    case 0x000E:
        sprintf(assm, "\tSHL\tV%u\n", (*opcode & 0x0F00U) >> 8U);
        break;
    
    default:
        sprintf(assm, "\tUNK=%04X\n", *opcode);
        break;
    }


    return;
}


void get_instruction_e(uint16_t* opcode, char* assm)
{
    switch (*opcode & 0x00FF)
    {
    case 0x009E:
        sprintf(assm, "\tSKP\tV%u\n", (*opcode & 0x0F00U) >> 8U);
        break;  

    case 0x00A1:
        sprintf(assm, "\tSKNP\tV%u\n", (*opcode& 0x0F00U) >> 8U);
        break;
    
    default:
        sprintf(assm, "\tUNK=%04X\n", *opcode);
        break;
    }


    return;
}


void get_instruction_f(uint16_t* opcode, char* assm)
{
    switch (*opcode & 0x00FFU)
    {
    case 0x0007:
        sprintf(assm, "\tLD\tV%u, DT\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x000A:
        sprintf(assm, "\tLD\tV%u, K\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x0015:
        sprintf(assm, "\tLD\tDT, V%u\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x0018:
        sprintf(assm, "\tLD\tST, V%u\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x001E:
        sprintf(assm, "\tADD\tINDEX, V%u\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x0029:
        sprintf(assm, "\tLD\tINDEX-FONT, V%u\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x0033:
        sprintf(assm, "\tLD\tINDEX-BCD, V%u\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x0055:
        sprintf(assm, "\tLD\t[I], V0toV%u\n", (*opcode & 0x0F00U) >> 8U);
        break;

    case 0x0065:
        sprintf(assm, "\tLD\tV0toV%u, [I]\n", (*opcode & 0x0F00U) >> 8U);
        break;
    
    default:
        sprintf(assm, "\tUNK=%04X\n", *opcode);
        break;
    }


    return;
}